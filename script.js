function Person(firstName, lastName){
	this.firstName = firstName;
	this.lastName = lastName;


	Person.prototype.sayName = function (){
		console.log(`Hi my name is ${this.firstName} ${this.lastName}`)
	}

}

let personA = new Person ('Alan', 'Beraquit')
personA.sayName()